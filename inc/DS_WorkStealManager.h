#pragma once
#include "DS_definitions.h"
//#include "DS_common_def.h"
#include "DS_lock.h"

/**
This is a class that manages the work stealing among multiple threads
@author Duksu Kim (bluekdct at gmail dot com
@date	09/09/2016
*/
class DS_WorkStealManager
{
protected:
	int numThreads;
	bool** requestFlag;
	bool* finishedFlag;

	DS_lock lock;

public:
	DS_WorkStealManager(int _numThreads = 1);
	~DS_WorkStealManager();

	/** Initialize all status on this manager */
	void initManager(void);

	/** Check whether all threads are finished there work or not */
	bool isAllFinished(void);

	/** notifiy the state of the thread #_tID*/
	void setFinished(UINT _tid);

	/** notifiy the state of the thread #_tID*/
	void unsetFinished(UINT _tid);

	/** Send work request from the '_from' thread to the '_to' thread
	*/
	void sendRequest(UINT _from, UINT _to);

	/** Get the recieved request
	@param	_tid	the request queue ID
	@return			the thread ID who sends the request
	*/
	int checkRequest(UINT _tid);

	/** Check whether the thread _tID finished all given work or not */
	bool isFinished(UINT _tID) { return finishedFlag[_tID]; }

};

