#pragma once

#include "DS_lock.h"
#include <stdio.h>

#define DS_HASH_FUNC_DIVISION		0
#define DS_HASH_FUNC_KNUTH_DIV		3
#define	DS_HASH_FUNC_MULTIPLICATION	1

class DS_hashedLock
{
private:
	unsigned int	numLocks ;
	DS_lock*		locks;

	unsigned int (DS_hashedLock::*hashFunc) (unsigned int);

	//*** hash functions ***/
	// Ref: https://www.cs.hmc.edu/~geoff/classes/hmc.cs070.200101/homework10/hashfuncs.html

	/** Division hash : h(k) = k mod m */
	unsigned int hash_div(unsigned int _in);
	/** Knuth Variant on Division : h(k) = k(k+3) mod m */
	unsigned int hash_knuth_div(unsigned int _in);

	/** Multiplication Method Choose m to be a power of 2 */
	unsigned int hash_multiplication(unsigned int _in){
		// TODO
		return 0;
	}

public:
	DS_hashedLock(unsigned int _numLock = 1) {
		numLocks = _numLock;
		locks = new DS_lock(numLocks);
		setHashFunc();
	}

	~DS_hashedLock(void){
		delete locks;
	}

	int setHashFunc(int _funID = DS_HASH_FUNC_DIVISION);

	void initLocks(void) { locks->initAllLock(); }
	void setLock(unsigned int _id) { locks->setLock((this->*hashFunc)(_id)); }
	void unsetLock(unsigned int _id) { locks->unsetLock((this->*hashFunc)(_id)) ;}
} ;