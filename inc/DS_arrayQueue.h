#pragma once

#include <omp.h>
#include "DS_Queue.h"

#define	DIRECTION_BACK	true
#define DIRECTION_FRONT	false

#define DS_QUEUE_DEFAULT_SIZE	1000

// Note: This code do not consider queue overflow

template <class T>
class DS_arrayQueue : public DS_Queue
{
private:

	T			*q ;
	int			capacity ;
	int			size ;
	/** The index of the non-empty slot at most front side */
	int			front ;
	/** The index of the empty slot at most back side */
	int			back ;

	bool		isSynchAccess ;
	omp_lock_t	lock ;

	inline void setLock ( void ) {
		if ( isSynchAccess)
			omp_set_lock (&lock) ;
	}
	inline void unsetLock ( void ) {
		if ( isSynchAccess)
			omp_unset_lock(&lock) ;
	}

public:
	DS_arrayQueue( UINT _size = DS_QUEUE_DEFAULT_SIZE, bool _useCuda = false, bool _isSynchAccess = false ) ;
	~DS_arrayQueue(void);

	/************************************************************************/
	/* Virtual member methods                                               */
	/************************************************************************/
	inline virtual UINT sizeOfelement( void ) { return sizeof(T) ; }
	inline virtual UINT getCapacity ( void ) { return capacity ; }

	inline virtual bool push_back( void* data ) { return push_back((T*)data) ; }
	inline virtual bool pop_front( void* data ) { return pop_front((T*)data) ;  }
	inline virtual bool pop_back( void* data ) { return pop_back((T*)data) ; }

	inline virtual bool	getChunk_back ( UINT chunkSize, void** startPoint ) {return getChunk_back(chunkSize, (T**)startPoint) ; }
	inline virtual bool	getChunk_front ( UINT chunkSize, void** startPoint ) { return getChunk_front(chunkSize, (T**)startPoint) ; }
	inline virtual bool	putChunk_back ( UINT chunkSize, void* chunk ) { return putChunk_back(chunkSize, (T*)chunk) ; }
	inline virtual bool	readAt ( UINT index, void* dest ) { return readAt(index, (T*)dest) ; }

	virtual int	compaction ( void ) ;

	/************************************************************************/
	/* Actual definitions                                                   */
	/************************************************************************/

	void init() ;

	bool push_back( T* data ) ;
	bool pop_front( T* data ) ;
	bool push_front( T* data ) ;
	bool pop_back( T* data ) ;

	inline bool getFront( T* _dst ) {
		if ( isEmpty() ) return false ;
		*_dst = q[front] ;
		return false ;
	}
	inline T getBack( void ) {
		if ( isEmpty() ) return false ;
		*_dst = q[back-1] ;
		return true ;
	}

	inline int getFrontIndex (void) { return front ; }
	inline int getBackIndex (void) { return back ; }

	inline bool pop_stack ( T* data ) { return pop_back(data)	;}
	inline bool push_stack ( T* data) { return push_back(data) ; }

	inline bool pop_queue ( T* data ) { return pop_front(data)	; }
	inline bool push_queue ( T* data ){ return push_back(data) ; }

	inline UINT Size ( void ) { return size ;}
	inline bool isEmpty ( void ) { return ( size == 0 ) ; }

	inline bool isFull ( void ) { return (size == capacity) ; }
	inline bool isFull_back ( void ) { return ( capacity < back ) ; }
	inline bool isFull_front ( void ) { return (front == 0) ; }

	inline UINT remainSpace ( void ) { return capacity - back ; }

	/**
	Pop a set of elements (chunk) from back of the queue
	@param chunkSize	The size of array to pop
	@param startPoint	Indicate the start point of the popped elements
	@return				true : success to get, false : fail to pop
	*/
	bool	getChunk_back ( UINT chunkSize, T** startPoint ) ;

	/**
	Pop a set of elements (chunk) from front of the queue
	@param chunkSize	The size of array to pop
	@param startPoint	Indicate the start point of the popped elements
	@return				true : success to get, false : fail to pop
	*/
	bool	getChunk_front ( UINT chunkSize, T** startPoint ) ;

	/**
	Push a set of elements (chunk) to the back of the queue
	@param chunkSize	The size of chuck to push
	@param chunk		The pointer that indicate the array of the chunk to push
	@return				true : success to push, false : fail to push
	*/
	bool	putChunk_back ( UINT chunkSize, T* chunk ) ;

	///**
	//Push a set of elements (chunk) to the front of the queue
	//@param chunkSize	The size of chuck to push
	//@param chunk		The pointer that indicate the array of the chunk to push
	//@return				true : success to push, false : fail to push
	//*/
	//bool	putChunk_front ( UINT chunkSize, T* chunk ) ;

	/**
	*/
	bool	readAt ( UINT index, T* dest ) ;
	
	inline T* getQueueStart ( void ) { return q ; }

	/**
	@return the size of the queue after moving the back
	*/
	inline int moveBack ( int _offset ) {
		( back + _offset > capacity ? back = capacity : back += _offset ) ;
		size = back - front ;
		return size ;
	}
};

/************************************************************************/
/*                                                                      */
/************************************************************************/
template <class T>
DS_arrayQueue<T>::DS_arrayQueue( UINT _size, bool _useCuda, bool _isSynchAccess ){
	if (_size > INT_MAX ) {
		printf("DS_arrayQueue support up to the size of %d. The requested size is %d.", INT_MAX, size) ;
		printf("Press any key to exit...") ;
		getchar() ;
	}

	isSynchAccess = _isSynchAccess ;

	UseCuda(_useCuda) ;
	capacity = _size ;
	DSQ_MEM_ALLOC<T>( &q, capacity ) ;
	init() ;

	omp_init_lock(&lock) ;
}

template <class T>
DS_arrayQueue<T>::~DS_arrayQueue(void){
	DSQ_MEM_RELEASE(q) ;
	omp_destroy_lock(&lock) ;
}

template <class T>
void DS_arrayQueue<T>::init(){
	front = 0 ; back = 0 ; size = 0 ;
}

template <class T>
bool DS_arrayQueue<T>::push_back( T* data ){

	setLock() ;

	if ( isFull_back() ) {
		unsetLock() ;
		return false ;
	}

	q[back] = *data ;
	back++ ;

	size++ ;

	unsetLock() ;

	return true ;
}

template <class T>
bool DS_arrayQueue<T>::pop_back( T* data ){

	setLock() ;

	if ( isEmpty() ) {
		unsetLock() ;
		return false ;
	}

	back-- ;
	*data = q[back] ;

	size-- ;

	unsetLock() ;

	return true ;
}


template <class T>
bool DS_arrayQueue<T>::push_front( T* data )
{
	setLock() ;

	if ( isFull_front() ) {
		unsetLock() ;
		return false ;
	}

	front-- ;
	q[front] = *data ;

	size++ ;

	unsetLock() ;
	return true ;
}


template <class T>
bool DS_arrayQueue<T>::pop_front( T* data ){

	setLock() ;

	if ( isEmpty() ) {
		unsetLock() ;
		return false ;
	}

	*data = q[front] ;
	front++ ;
	
	size-- ;

	unsetLock() ;

	return true ;
}

template <class T>
bool DS_arrayQueue<T>::getChunk_back( UINT chunkSize, T** startPoint ) {

	setLock() ;

	if ( Size() < chunkSize ) {
		unsetLock() ;
		return false ;
	}

	back = back - chunkSize ;
	//memcpy(*startPoint, &q[back], sizeof(T)*chunkSize) ;
	*startPoint = &q[back] ;

	size -= chunkSize ;

	unsetLock() ;

	return true ;
}

template <class T>
bool DS_arrayQueue<T>::getChunk_front( UINT chunkSize, T** startPoint )
{
	setLock() ;

	if ( Size() < chunkSize ) {
		unsetLock() ;
		return false ;
	}

	//memcpy(*startPoint, &q[front], sizeof(T)*chunkSize) ;
	*startPoint = &q[front] ;
	front = front + chunkSize ;

	size -= chunkSize ;

	unsetLock() ;

	return true ;
}

template <class T>
bool DS_arrayQueue<T>::putChunk_back( UINT chunkSize, T* chunk ) {

	setLock() ;

	if ( remainSpace() < chunkSize ) {
		unsetLock() ;
		return false ;
	}

	memcpy(&q[back], chunk, sizeof(T)*chunkSize ) ;
	back += chunkSize ;

	size += chunkSize ;

	unsetLock() ;

	return true ;
}

template <class T>
int DS_arrayQueue<T>::compaction( void )
{
	setLock() ;

	if ( front < 0 ) {
		unsetLock() ;
		return remainSpace() ;
	}

	int emptyInFront = front + 1 ;
	back = back - emptyInFront ;
	memcpy(q, &q[back], sizeof(T)*emptyInFront) ;

	front = 0 ;
	size = back ;

	unsetLock() ;

	return remainSpace() ;
}


template <class T>
bool DS_arrayQueue<T>::readAt( UINT index, T* dest )
{
	setLock() ;

	if ( Size() <= index ) {
		unsetLock() ;
		return false ;
	}

	*dest =	q[front+index] ;
	unsetLock() ;

	return true ;
}
