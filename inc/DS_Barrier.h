#pragma once

/**
Barrier for the spin-lock synchronization
*/
class DS_Barrier
{
	int dumy ;
	int numThreads ;
	bool masterFlag ;
	bool* flag ;

	bool isAllreach ( void ) { return masterFlag ; }
	bool isAllPass ( void ) { return !masterFlag ; }
	void checkAllReach ( void ) ;
	void checkAllPass ( void ) ;

	void passBarrier ( int tID ) ;
public:
	/**
	*/
	DS_Barrier(int _numThreads);
	/**
	*/
	~DS_Barrier(void);

	/**
	*/
	void reachBarrier ( int tID ) ;
	/**
	*/
	void initBarrier ( void ) ;
} ;
