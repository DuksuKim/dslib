#pragma once

#include <omp.h>
#include "DS_definitions.h"
#include "DS_Library_Cuda.h"

#define DS_CIRCULARQ_DEFAULT_SIZE	100

template <class T>
class DS_circularQ
	: public DS_Library_Cuda
{
private :

	T*		data ;
	int		capacity ;
	int		size ;
	int		front ;
	int		rare ;

	omp_lock_t lock ;

	inline void setLock ( void ) { omp_set_lock ( &lock) ; }
	inline void unsetLock ( void ) { omp_unset_lock( &lock) ; }

public :
	DS_circularQ( int maxSize = DS_CIRCULARQ_DEFAULT_SIZE, bool _useCuda = false ) ;
	~DS_circularQ() ;

	void init() ;
	bool push( T* newData ) ;
	bool pop ( T* dest ) ;
	bool getFront ( T* dest ) ;
	bool getBack ( T* dest ) ;

	inline int getSize (void) {
		int re = 0 ;
		setLock() ;
		re = size ;
		unsetLock() ;
		return re ;
	}

	inline bool isFull ( void ) {
		bool re = 0 ;
		setLock() ;
		re = (size == capacity) ;
		unsetLock() ;
		return re ;
	}
	inline bool isEmpty ( void ) {
		bool re = 0 ;
		setLock() ;
		re = (size == 0) ;
		unsetLock() ;
		return re ;
	}

} ;

/************************************************************************/
/*                                                                      */
/************************************************************************/

template <class T>
DS_circularQ<T>::DS_circularQ( int maxSize, bool _useCuda )
{
	UseCuda(_useCuda) ;

	init() ;
	omp_init_lock(&lock) ;

	capacity = maxSize ;
	DSQ_MEM_ALLOC<T>(&data, capacity) ;
}

template <class T>
DS_circularQ<T>::~DS_circularQ()
{
	omp_destroy_lock(&lock) ;
	DS_MEM_DELETE_ARRAY(data) ;
}

template <class T>
void DS_circularQ<T>::init()
{
	front = 0 ;
	rare = 0 ;
	size = 0 ;
}

template <class T>
bool DS_circularQ<T>::push( T* newData )
{
	setLock() ;
	if ( size == capacity ) {
		unsetLock() ;
		return false ;
	}
	data[rare] = *newData ;
	rare = (rare + 1) %capacity ;
	size++ ;

	unsetLock() ;
	return true ;
}

template <class T>
bool DS_circularQ<T>::pop( T* dest )
{
	setLock() ;
	if ( (size == 0) ) {
		unsetLock() ;
		return false ;
	}

	*dest = data[front] ;
	front = (front + 1)%capacity ;
	size-- ;

	unsetLock() ;
	return true ;
}
template <class T>
bool DS_circularQ<T>::getFront( T* dest )
{
	setLock() ;
	if ( (size == 0) ) {
		unsetLock() ;
		return false ;
	}
	*dest = data[front] ;
	unsetLock() ;
	return true ;
}

template <class T>
bool DS_circularQ<T>::getBack( T* dest )
{
	setLock() ;
	if ( (size == 0) ) {
		unsetLock() ;
		return false ;
	}
	dest = data[(rare-1)%capacity] ;
	unsetLock() ;
	return true ;
}