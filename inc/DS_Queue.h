#pragma once

#include "DS_definitions.h"
//#include "DS_common_def.h"
#include "DS_Library_Cuda.h"

class DS_Queue
	: public DS_Library_Cuda
{
//protected:
//	bool	useCuda ;

public:
	DS_Queue(void) {} ;
	~DS_Queue(void) {} ;

	/** Get the size (byte) of an element */
	virtual UINT sizeOfelement ( void  ) = 0 ;
	/** Get the full capacity of this queue */
	virtual UINT getCapacity ( void ) = 0 ;
	/** Get the number of elements in this queue */
	virtual UINT Size ( void ) = 0 ;

	/** Query whether this queue is empty or not */
	virtual bool isEmpty ( void ) = 0 ;
	/** Query whether this queue is full or not */
	virtual bool isFull ( void ) = 0 ;
	/** Get the remaining capacity of this queue */
	virtual UINT remainSpace ( void ) = 0 ;
	/** Compact this queue
	@return The size of remaining space of the queue after compaction
	*/
	virtual int compaction ( void ) = 0 ;

	/** Push a element to the back of this queue */
	virtual bool push_back ( void* data ) = 0 ;
	/** Pop a element from the back of this queue */
	virtual bool pop_back( void* data ) = 0 ;
	/** Pop a element from the front of this queue */
	virtual bool pop_front( void* data ) = 0 ;
	///** Push a element to the front of this queue */
	//virtual bool push_front( void* data ) = 0 ;

	/**
	Pop a set of elements (chunk) from back of the queue
	@param chunkSize	The size of array to pop
	@param startPoint	Indicate the start point of the popped elements
	@return				true : success to get, false : fail to get
	*/
	virtual bool	getChunk_back ( UINT chunkSize, void** startPoint ) = 0 ;

	/**
	Pop a set of elements (chunk) from front of the queue
	@param chunkSize	The size of array to pop
	@param startPoint	Indicate the start point of the popped elements
	@return				true : success to get, false : fail to get
	*/
	virtual bool	getChunk_front ( UINT chunkSize, void** startPoint ) = 0 ;

	/**
	Push a set of elements (chunk) to the back of the queue
	@param chunkSize	The size of chuck to push
	@param chunk		The pointer that indicate the array of the chunk to push
	@return				true : success to push, false : fail to push
	*/
	virtual bool	putChunk_back ( UINT chunkSize, void* chunk ) = 0 ;

	///**
	//Push a set of elements (chunk) to the front of the queue
	//@param chunkSize	The size of chuck to push
	//@param chunk		The pointer that indicate the array of the chunk to push
	//@return				true : success to push, false : fail to push
	//*/
	//virtual bool	putChunk_front ( UINT chunkSize, void* chunk ) = 0 ;

	/**
	Read data at index
	@param	index	Data index to read
	@param	dest	Destination to store the read data
	@return			true: success to read, false: fail to read
	*/
	virtual bool	readAt ( UINT index, void* dest ) = 0 ;
};
