#pragma once


#include <cuda_runtime_api.h>
#include <helper_cuda.h>

#define _DS_LIB_CUDA_SAFE			checkCudaErrors // cutilSafeCall		/* for older than CUDA 5.0 */
#define _DS_LIB_CUDA_SAFE_NoSync	checkCudaErrors // cutilSafeCallNoSync	/* for older than CUDA 5.0 */

class DS_Library_Cuda
{
public:
	bool useCuda ;

	bool UseCuda() const { return useCuda; }
	void UseCuda(bool val) { useCuda = val; }

	template <typename T>
	inline void DSQ_MEM_ALLOC ( T** p, int size ) {
		if ( useCuda ) {
			_DS_LIB_CUDA_SAFE ( cudaHostAlloc((void**)p, sizeof(T)*size, cudaHostAllocPortable) ) ;
		}
		else
		{
			*p = new T[size] ;
		}
	}

	#define DSQ_MEM_RELEASE(p) {			\
		if(useCuda){						\
		_DS_LIB_CUDA_SAFE ( cudaFreeHost(p) ) ;	\
		p = NULL ;							\
		} else {							\
		if ( p != NULL ) { delete [] p ; p = NULL ;	} \
		}									\
	}
} ;