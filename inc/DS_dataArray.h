#pragma once

#include "DS_definitions.h"
//#include "DS_common_def.h"

#define DS_DATA_ARRAY_DEFAULT_NUM_TUPLES	512

template <class T>
class DS_dataArray
{
private:
	/** Actual data array */
	T* data;

	// Configurations

	/** The # of components in a tuple */
	int numOfComponents;
	/** The # of tupels */
	int numOfTuples;

	/** is this object used in GPU?*/
	bool isForGPU;
	unsigned int gpuID;

	// status
	
	/** Indicator for last tuple  */
	int lastTuple;
	long long sizeOfAllocaedMememory;

private:
	bool allocMemory();
	bool releaseMemory();

	bool allocMemCPU();
	bool releaseMemCPU;

	bool alocMemGPU();
	bool releaseMemGPU();

public:
	DS_dataArray(int _numComp = 1, int _numTuples = DS_DATA_ARRAY_DEFAULT_NUM_TUPLES
				, bool _inGPU = false, int _gID = 0);
	~DS_dataArray();
};

/************************************************************************/
/*                                                                      */
/************************************************************************/

template <class T>
DS_dataArray<T>::DS_dataArray(int _numComp, int _numTuples, bool _isForGPU, int _gID)
	: data(NULL), lastTuple(-1), sizeOfAllocaedMememory(0), isForGPU(_isForGPU), gpuID(_gID)
{
	numOfComponents = _numComp;
	numOfTuples = _numTuples;
	allocMemory();
}

template <class T>
DS_dataArray<T>::~DS_dataArray()
{
	releaseMemory();
}

template <class T>
bool DS_dataArray<T>::allocMemory(){
	releaseMemory();
	sizeOfAllocaedMememory = numOfComponents * numOfTuples * sizeof(T);
	return (isForGPU ? allocMemGPU() : allocMemCPU());
}

template <class T>
void DS_dataArray<T>::releaseMemory(){
	if ((isForGPU ? releaseMemGPU() : releaseMemCPU())) {
		data = NULL;
		sizeOfAllocaedMememory = 0;
		return true;
	}
	return false;
}

template <class T>
void DS_dataArray<T>::allocMemCPU()
{

}

template <class T>
void DS_dataArray<T>::releaseMemCPU()
{

}

template <class T>
void DS_dataArray<T>::allocMemGPU()
{
	cudaSetDevice(gID);
}

template <class T>
void DS_dataArray<T>::releaseMemGPU()
{
	cudaSetDevice(gID);
}