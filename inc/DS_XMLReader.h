#pragma once

#include <vtkXMLDataParser.h>
#include <vtkXMLDataElement.h>

/**
XML reader & parser
@author Duksu Kim (bluekdct@gmail.com)
*/
class DS_XmlReader
{
private:
	std::string inputFileName;

	vtkXMLDataParser *parser;
	vtkXMLDataElement* xmlRootElement;

private:
	void trimString(char* _str);
	void MakeUpperString(char* _str);
	vtkXMLDataElement* safe_GetElementWithName(vtkXMLDataElement* _curEle, char* _name);

public:
	/**	Read a XML file and do initial parsing
	@param _inputFileName	File name to be read
	*/
	DS_XmlReader(const char* _inputFileName);
	~DS_XmlReader();

	/** Return the root elements of the XML file */
	vtkXMLDataElement* getRootElement(void) { return xmlRootElement; }
	/**
	@param _curEle	The current element that may be a parent element of the target element
	@param _name	The name of the target element to find
	@return			The target element whoes name is _name. If it cannot find the target element, it returns NULL.
	*/
	vtkXMLDataElement* getNestedElementWithName(vtkXMLDataElement* _curEle, char* _name)
		{ return _curEle->FindNestedElementWithName(_name); }

	/** Find the target element and return the value as a boolean value
	@param _curEle	The current element that may be a parent element of the target element
	@param _dest	The result value is returned to this variable
	@param _key		The name of the target elements. If it is NULL, the target is _curEle
	@return			If it find the target element, it returns true. otherwise, it returns false
	*/
	bool loadDataBool(vtkXMLDataElement* _curEle, bool& _dest, char* _key = NULL);
	/** _curEle is the root node */
	bool loadDataBool(bool& _dest, char* _key)
		{ return loadDataBool(xmlRootElement, _dest, _key); }

	/** Find the target element and return the value as an integer value
	@param _curEle	The current element that may be a parent element of the target element
	@param _dest	The result value is returned to this variable
	@param _key		The name of the target elements. If it is NULL, the target is _curEle
	@return			If it find the target element, it returns true. otherwise, it returns false
	*/
	bool loadDataInt(vtkXMLDataElement* _curEle, int& _dest, char* _key = NULL);
	/** _curEle is the root node */
	bool loadDataInt(int& _dest, char* _key)
		{return loadDataInt(xmlRootElement, _dest, _key); }

	/** Find the target element and return the value as a float value
	@param _curEle	The current element that may be a parent element of the target element
	@param _dest	The result value is returned to this variable
	@param _key		The name of the target elements. If it is NULL, the target is _curEle
	@return			If it find the target element, it returns true. otherwise, it returns false
	*/
	bool loadDataFloat(vtkXMLDataElement* _curEle, float& _dest, char* _key = NULL);
	/** _curEle is the root node */
	bool loadDataFloat(float& _dest, char* _key)
		{ return loadDataFloat(xmlRootElement, _dest, _key); }

	/** Find the target element and return the value as a string
	@param _curEle	The current element that may be a parent element of the target element
	@param _dest	The result value is returned to this variable
	@param _key		The name of the target elements. If it is NULL, the target is _curEle
	@return			If it find the target element, it returns true. otherwise, it returns false
	*/
	bool loadDataString(vtkXMLDataElement* _curEle, char* _dest, char* _key = NULL);
	/** _curEle is the root node */
	bool loadDataString(char* _dest, char* _key)
		{ return loadDataString(xmlRootElement, _dest, _key); }
};