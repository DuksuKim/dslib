#ifndef _DS_TIMER_H
#define _DS_TIMER_H

#include <string> // std string

#ifndef UINT
typedef unsigned int UINT;
#endif

#ifdef _WIN32
	// For windows
	#include <Windows.h>
	typedef LARGE_INTEGER	TIME_VAL;
#else
	// For Unix/Linux
	#include <stdio.h>
	#include <stdlib.h>
	#include <sys/time.h>
	#include <string.h>	// c string
	typedef struct timeval	TIME_VAL;
#endif

#define TIMER_ON	true
#define TIMER_OFF	false

// macro functions
#define DSTIMER_SET_TIMER_NAME(_timer,_id_name) \
			_timer->setTimerName(_id_name,#_id_name);

/**
DS Time checker & counter
@author Duksu Kim (bluekdct@gmail.com)
*/
class DS_timer
{
private :

	bool turnOn	;

	UINT numTimer	;
	UINT numCounter ;

	// For timers
	bool*			timerStates ;
	TIME_VAL	ticksPerSecond;
	TIME_VAL	*start_ticks;
	TIME_VAL	*end_ticks;
	TIME_VAL	*totalTicks;

	char		timerTitle[255] ;
	std::string *timerName ;
	std::string *counterName ;

	// For counters
	UINT *counters ;

	void memAllocCounters ( void ) ;
	void memAllocTimers ( void ) ;
	void releaseCounters ( void ) ;
	void releaseTimers ( void ) ;

	void timerAdd(UINT id, TIME_VAL _time);

public:
	DS_timer(int _numTimer = 1, int _numCount = 1, bool _trunOn = true );
	~DS_timer(void);

	// For configurations
	inline void timerOn ( void ) { turnOn = TIMER_ON ; }
	inline void timerOff ( void ) { turnOn = TIMER_OFF ; }

	UINT getNumTimer( void ) ;
	UINT getNumCounter ( void ) ;

	/** Set the number of timers*/
	UINT setNumTimer ( UINT _numTimer ) ;
	/** Set the number of counters */
	UINT setNumCounter ( UINT _numCounter ) ;

	// For timers
	void initTimer(UINT id) ;
	void initTimers ( void );
	void onTimer(UINT id) ;
	void offTimer(UINT id ) ;

	void setTimer(UINT id, TIME_VAL _tick);
	void setTimer(UINT id, double _ms) { setTimer(id, ms2tick(_ms)); }

	double getTimer_ms(UINT id) ;
	TIME_VAL getTimer_tick(UINT id) ;

	void setTimerTitle ( char* _name ) { memset(timerTitle, 0, sizeof(char)*255) ; memcpy(timerTitle, _name, strlen(_name)) ; }

	void setTimerName (UINT id, std::string &_name) { timerName[id] = _name ; }
	void setTimerName (UINT id, char* _name) { timerName[id] = _name ;}

	// For counters
	void incCounter(UINT id) ;
	void initCounters( void ) ;
	void initCounter(UINT id) ;
	void add2Counter( UINT id, UINT num ) ;
	UINT getCounter ( UINT id ) ;

	void setCounterName(UINT id, std::string &_name) { counterName[id] = _name; }
	void setCounterName(UINT id, char* _name) { counterName[id] = _name; }

	// For reports
	void printTimer ( float _denominator = 1 ) ;
	/**
	@param _fileName	output file name
	@param _id			if (>= 0), the _id is printed as the first column
	@param _printAllTimer	if false, the timers=0 is not printed
	@param _printAllCounter	if false, the count=0 is not printed
	*/
	void printToFile ( char* _fileName, int _id = -1
					, bool _printAllTimer = false, bool _printAllCounter = false) ;
	void printTimerNameToFile ( char* fileName ) ;

public:	// Utilies (static methods)

	// converters
	static double tick2ms(TIME_VAL _totalTick);
	static TIME_VAL ms2tick(double _ms);

	static void timerAdd(UINT id, TIME_VAL _time, TIME_VAL* _dest);

	static TIME_VAL timerSum_tick(DS_timer* _subTimers, int _numTimers, int _timerID = 0);
	static TIME_VAL timerAvg_tick(DS_timer* _subTimers, int _numTimers, int _timerID = 0);

	/**
	@param	_subTimers		a list of timers to handle
	@param	_numTimers		the number of timers to handle
	@param	_timerID		the target ID of the timers
	@return					the sumation of all timers in milliseconds
	*/
	static double timerSum_ms(DS_timer* _subTimers, int _numTimers, int _timerID = 0);
	/**
	@param	_subTimers		a list of timers to handle
	@param	_numTimers		the number of timers to handle
	@param	_timerID		the target ID of the timers
	@return					the average time in milliseconds
	*/
	static double timerAvg_ms(DS_timer* _subTimers, int _numTimers, int _timerID = 0);

	/**
	@param	_subTimers		a list of timers to handle
	@param	_numTimers		the number of timers to handle
	@param	_timerID		the target counter ID of the timers
	@return					the summation of counters
	*/
	static long counterSum(DS_timer* _subTimers, int _numTimers, int _counterID = 0);
	/**
	@param	_subTimers		a list of timers to handle
	@param	_numTimers		the number of timers to handle
	@param	_timerID		the target counter ID of the timers
	@return					the average value of counters
	*/
	static double counterAvg(DS_timer* _subTimers, int _numTimers, int _counterID = 0);
} ;
	
#endif