#ifndef _CUDA_DEFINITIONS_H_
#define _CUDA_DEFINITIONS_H_

//#include <cuda.h>
#include <cuda_runtime_api.h>
#include <helper_cuda.h>

/************************************************************************/
/* Definitions                                                          */
/************************************************************************/
#define CUDA_SAFECALL checkCudaErrors
#define __HnD__ __host__ __device__

// Block ID
#define	BID_X	blockIdx.x
#define	BID_Y	blockIdx.y
#define	BID_Z	blockIdx.z

// Thread ID
#define	TID_X	threadIdx.x
#define	TID_Y	threadIdx.y
#define	TID_Z	threadIdx.z

// Dimension of a grid
#define Gdim_X	gridDim.x
#define Gdim_Y	gridDim.y
#define Gdim_Z	gridDim.z

// Dimension of a block
#define Bdim_X	blockDim.x
#define Bdim_Y	blockDim.y
#define Bdim_Z	blockDim.z

// Indicators
#define FIRST_T_IN_BLCOK	(TID_X + TID_Y + TID_Z == 0)
#define LAST_T_IN_BLOCK		(TID_X == (Bdim_X-1) &&  TID_Y == (Bdim_Y-1) && TID_Z == (Bdim_Z-1))

#define FIRST_BLOCK		(BID_X + BID_Y + BID_Z == 0)
#define LAST_BLOCK		(BID_X == (Gdim_X-1) && BID_Y == (Gdim_Y-1) && BID_Z == (Gdim_Z-1))

#define FIRST_THREAD	(FIRST_BLOCK && FIRST_T_IN_BLCOK)
#define LAST_THREAD		(LAST_BLOCK && LAST_T_IN_BLOCK)

// The number of threads (TPB: threads per block)
#define NUM_TPB_X	(Bdim_X*Bdim_Y*Bdim_Z)
#define NUM_TPB_Y	(Gdim_X*NUM_TPB_X)
#define	NUM_TPB_Z	(Gdim_Y*NUM_TPB_Y)

#define NUM_TPB				(Bdim_X*Bdim_Y*Bdim_Z)
#define NUM_BLOCKS			(Gdim_X*Gdim_Y*Gdim_Z)
#define NUM_KERNEL_THREADS	(NUM_BLOCKS*NUM_TPB)

// Thread ID
#define TID_IN_BLOCK	(TID_Z*(Bdim_Y*Bdim_X) + TID_Y*Bdim_X + TID_X)
#define TID_GLOBAL		(BID_Z*NUM_TPB_Z + BID_Y*NUM_TPB_Y + BID_X*NUM_TPB_X + TID_IN_BLOCK)

/************************************************************************/
/* Macros                                                               */
/************************************************************************/
inline void setGPU(int _gID) { cudaSetDevice(_gID) ; }

template<class T>
inline void dMemAlloc (T** _ptr, int _size, double *_memUsage, int _gID = 0 ) {
	setGPU(_gID) ;
	CUDA_SAFECALL(cudaMalloc((void**)_ptr, sizeof(T)*_size)) ;
	if (_memUsage != NULL)
		*_memUsage += sizeof(T)*_size ;
}


template<class T>
inline void dMemHostAlloc (T** _ptr, int _size, double *_memUsage ) {
	//setGPU(_gID) ;
	CUDA_SAFECALL(cudaHostAlloc((void**)_ptr, sizeof(T)*_size, cudaHostAllocPortable)) ;
	if (_memUsage != NULL)
		*_memUsage += sizeof(T)*_size ;
}

template<class T>
inline void dMemSetZero (T** _ptr, int _size, int _gID = 0) {
	setGPU(_gID) ;
	CUDA_SAFECALL(cudaMemset(*_ptr, 0, sizeof(T)*_size)) ;
}

template<class T>
inline void dMemSetZeroAsync (T** _ptr, int _size, int _gID = 0, cudaStream_t _stream = 0) {
	setGPU(_gID) ;
	CUDA_SAFECALL(cudaMemsetAsync(*_ptr, 0, sizeof(T)*_size,_stream)) ;
}

inline void dPrintMemInfo(int _dID = 0) {
	size_t free, total;
	setGPU(_dID);
	cudaMemGetInfo(&free, &total);
	printf("[Device #%d] %lld bytes are available (total : %lld bytes)\n", _dID, free, total);
}

#define SAFE_DELETE_CUDA_HOST(p) {\
if (p != NULL) {\
CUDA_SAFECALL(cudaFreeHost(p)); p = NULL;}}

#define SAFE_DELETE_CUDA(p) {\
if (p != NULL) {\
CUDA_SAFECALL(cudaFree(p)); p = NULL;}}

#define MEMCPY_H_to_D(_dest,_src,_size) \
	CUDA_SAFECALL(cudaMemcpy(_dest,_src,_size,cudaMemcpyHostToDevice))

#define MEMCPY_H_to_D_Async(_dest,_src,_size,_stream) \
	CUDA_SAFECALL(cudaMemcpyAsync(_dest,_src,_size,cudaMemcpyHostToDevice,_stream))

#define MEMCPY_D_to_H(_dest,_src,_size) \
	CUDA_SAFECALL(cudaMemcpy(_dest,_src,_size,cudaMemcpyDeviceToHost))

#define MEMCPY_D_to_H_Async(_dest,_src,_size,_stream) \
	CUDA_SAFECALL(cudaMemcpyAsync(_dest,_src,_size,cudaMemcpyDeviceToHost,_stream))


#define LOOP_I(a) for(int i=0; i<a; i++)
#define LOOP_J(a) for(int j=0; j<a; j++)
#define LOOP_K(a) for(int k=0; k<a; k++)
#define LOOP_INDEX(index, end) for (int index = 0 ; index < end ; index++)
#define LOOP_INDEX_START_END(index, start, end) for (int index = start ; index < end ; index++)

#endif