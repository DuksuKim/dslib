#pragma once

#include "DS_definitions.h"
#include "DS_Library_Cuda.h"
#include <omp.h>

#define DS_SEG_ARRAY_DUBLE_BUFFER

// Initial configurations
#define DS_DEFAULT_SEGARRAY_SEG_SIZE	2048
#ifdef DS_SEG_ARRAY_DUBLE_BUFFER
	#define DS_DEFAULT_SEGARRAY_NUM_SEG		2048
#else
	#define DS_DEFAULT_SEGARRAY_NUM_SEG		4096
#endif

#define DS_DEFAULT_SEGARRAY_SIZE		(DS_DEFAULT_SEGARRAY_SEG_SIZE * DS_DEFAULT_SEGARRAY_NUM_SEG)

// Flag
#define	DS_SEG_EMPTY			0
#define DS_SEG_ALLOCATED		1
#define DS_SEG_FULL				2

/**
An array that is divided several segment. <br>
Each segments can be allocated to a thread
and each thread can work independently to each segment.<br><br>

== General workflow == <br>
	- For collecting <br>
		At first, each thread get empty segments by calling 'requestSeg'.
		Then, each thread works on allocated segments.
		When the segments are full, it reports the full-segment by calling 'returnFullSeg' <br>

	- For processing <br>
		'getFullSegs' -> Processing -> 'setEmptySeg'
*/

template <class T>
class DS_SegArray : public DS_Library_Cuda
{
private :
	// Configuration
	int arraySize	;
	int numSegment	;
	int sizeSegment	;

	int numThread	;

	T*		segArray	;	// Indicator

#ifdef DS_SEG_ARRAY_DUBLE_BUFFER
	T*		segSpace[2]	;	// Two space (used for compaction)
	bool	curSpace ;
#endif

	char*	flagSeg		;
	int*	segOwner	;

	bool*	segRequest	;

	int emptySeg		;	// Indicate the next empty segment that will be allocated to a thread

	T* pStart ;				// Indicate the start point of collected elements
	int	numElements	;		// The number of collected elements
	int curWaitingSeg ;		// Indicate the segment that is a front allocated segment 

	omp_lock_t lock ;
	void setLock(void) { omp_set_lock(&lock) ; }
	void unsetLock(void) { omp_unset_lock(&lock) ; }

	void updateNumElements ( void ) ;
	int finalGetElements ( T** _start ) ;

	// For each thread
	int*	indexInSeg ;
	T**		workingSeg	;
	T**		reservedSeg	;

public:
	/**
	Constructor
	@param -numThread	The number of thread that will use the segmented array
	@param _segSize		Size of a segment. A multiple of 2 is recommended
	@param _numSeg		The number of segments
	*/
	DS_SegArray ( int _numTread = 1, int _segSize = DS_DEFAULT_SEGARRAY_SEG_SIZE
				, int _numSeg = DS_DEFAULT_SEGARRAY_NUM_SEG, bool _useCuda = false ) ;
	/**
	Destructor
	*/
	~DS_SegArray(void) ;

	void init(void) ;

	void initialAlloc ( void ) ;

	bool alocSeg ( int _id ) ;
	void treatRequest ( void ) ;

	// Communication
	void requestSeg ( int _id ) ;
	void returnFullSeg ( int _segID ) ;
	void returnFullSeg ( T* _fullSeg ) ;
	void setEmptySeg ( int _startSeg, int _size ) ;

	int	getNumElements ( void ) ;
	int	getElements ( int size, T** _start ) ;
	bool isEmpty ( void ) ;

	int compaction (void) ;

	// For each thread
	inline bool pushElement ( int tID, T* _ele ) {
		if ( indexInSeg[tID] == sizeSegment ) {
			if ( reservedSeg[tID] != NULL ) {
				// Get a new segment
				returnFullSeg(workingSeg[tID]) ;
				workingSeg[tID] = reservedSeg[tID] ;
				indexInSeg[tID] = 0 ;
				reservedSeg[tID] = NULL ;
				requestSeg(tID) ;
			} else {
				return false ;
			}
		}

		workingSeg[tID][indexInSeg[tID]] = *_ele ;
		indexInSeg[tID]++ ;

		return true ;

	} // end pushElemet

	inline bool pushElement ( int tID, T _ele ) {
		if ( indexInSeg[tID] == sizeSegment ) {
			if ( reservedSeg[tID] != NULL ) {
				// Get a new segment
				returnFullSeg(workingSeg[tID]) ;
				workingSeg[tID] = reservedSeg[tID] ;
				indexInSeg[tID] = 0 ;
				reservedSeg[tID] = NULL ;
				requestSeg(tID) ;
			} else {
				return false ;
			}
		}

		workingSeg[tID][indexInSeg[tID]] = _ele ;
		indexInSeg[tID]++ ;

		return true ;

	} // end pushElemet

	inline UINT segIndexFromPointer ( T* _pointer ) {
		return (UINT)(( _pointer - segArray ) / sizeSegment ) ;
	}

	inline int ownerThread ( int _segID ) {
		return segOwner[_segID] ;
		/*
		int segIndex ;
		for ( int tID = 0 ; tID < numThread ; tID++ ) {
			segIndex = segIndexFromPointer(workingSeg[tID]) ;
			
			if ( _segID == segIndex )
				return tID ;

			segIndex = segIndexFromPointer(reservedSeg[tID]) ;
			
			if ( _segID == segIndex )
				return tID ;
		}

		return -1 ; // No owner
		*/
	}

} ;

/************************************************************************/
/* Const/Destructor & initializer                                       */
/************************************************************************/
template <class T>
DS_SegArray<T>::DS_SegArray(int _numTread, int _segSize, int _numSeg, bool _useCuda )
{
	UseCuda(_useCuda) ;
	sizeSegment = _segSize	;
	numSegment	= _numSeg	;
	arraySize	= sizeSegment * numSegment ;

	numThread = _numTread ;

#ifdef DS_SEG_ARRAY_DUBLE_BUFFER
	DSQ_MEM_ALLOC<T>(&segSpace[0], arraySize) ;
	DSQ_MEM_ALLOC<T>(&segSpace[1], arraySize) ;
	curSpace = 0 ;
	segArray = segSpace[curSpace] ;
#else
	DSQ_MEM_ALLOC<T>(&segArray, arraySize) ;
#endif

	flagSeg		= new char[numSegment] ;
	segOwner	= new int[numSegment] ;

	segRequest	= new bool[numThread] ; 

	omp_init_lock(&lock) ;

	// For each threads
	indexInSeg = new int[numThread] ;
	workingSeg = new T*[numThread] ;
	reservedSeg = new T*[numThread] ;

	init() ;
}

template <class T>
DS_SegArray<T>::~DS_SegArray(void)
{
	DSQ_MEM_RELEASE(segArray) ;

	DS_MEM_DELETE_ARRAY(flagSeg) ;
	DS_MEM_DELETE_ARRAY(segRequest) ;

	omp_destroy_lock(&lock) ;

	// For each thread
	DS_MEM_DELETE_ARRAY(indexInSeg) ;
	DS_MEM_DELETE_ARRAY(workingSeg) ;
	DS_MEM_DELETE_ARRAY(reservedSeg) ;
}

template <class T>
void DS_SegArray<T>::init( void )
{
	for ( int i = 0 ; i < numSegment ; i++ ) {
		flagSeg[i] = DS_SEG_EMPTY ;
		segOwner[i] = -1 ;
	}

	for ( int i = 0 ; i < numThread ; i++ ) {
		segRequest[i] = false ;
	}

	emptySeg	= 0	;

	pStart = segArray ;
	numElements = 0 ;
	curWaitingSeg = 0 ;

	numElements = 0 ;

	// Initial segments allocation
	initialAlloc() ;
}

/************************************************************************/
/* Methods for management                                                                     */
/************************************************************************/

template <class T>
void DS_SegArray<T>::updateNumElements( void ) {
	setLock() ;

	// Case 1. DS_SEG_ALLOCATED
	if ( flagSeg[curWaitingSeg] == DS_SEG_ALLOCATED ){
		//omp_unset_lock( &lock ) ;
		unsetLock() ;
		return ;
	}

	// Case 2. DS_SEG_FULL
	if ( flagSeg[curWaitingSeg] == DS_SEG_FULL ) {
		while ( flagSeg[curWaitingSeg] == DS_SEG_FULL ) {
			numElements += sizeSegment ;
			curWaitingSeg++ ;
		}
		unsetLock() ;
		return ;
	}

	// case 3. EMPTY
	unsetLock() ;

	return ;
}

template <class T>
void DS_SegArray<T>::initialAlloc( void )
{
	for ( int i = 0 ; i < numThread ; i++ ) {
		alocSeg( i ) ;
	}

	for ( int i = 0 ; i < numThread ; i++ ) {
		indexInSeg[i] = 0 ;
		workingSeg[i] = reservedSeg[i] ;
		reservedSeg[i]= NULL ;
	}

	for ( int i = 0 ; i < numThread ; i++ ) {
		alocSeg( i ) ;
	}
}

template <class T>
void DS_SegArray<T>::treatRequest( void ) {
	for ( int id = 0 ; id < numThread ; id++ ) {
		if ( segRequest[id] == true ) {

			if( alocSeg(id) )
				segRequest[id] = false ;
			else
				break ;
		}
	}

	updateNumElements() ;
}

template <class T>
bool DS_SegArray<T>::alocSeg( int _id )
{
	if ( emptySeg < 0 )
		return false ;	// no empty segment

	// Allocate a segment
	flagSeg[emptySeg] = DS_SEG_ALLOCATED ;
	segOwner[emptySeg] = _id ;

	reservedSeg[_id] = segArray + (emptySeg*sizeSegment) ;

	emptySeg++ ;
	if ( emptySeg >= numSegment ) {
		emptySeg = -1 ;
		printf("Overflow at seg_array.. Press any key to exit\n") ;
		exit(0) ;
	}

	return true ;
}

template <class T>
int DS_SegArray<T>::compaction( void )
{
#ifdef DS_SEG_ARRAY_DUBLE_BUFFER

	setLock() ;

	int sendSize = 0 ;

	//** Step 1. Threat the first segment in which the pStart is positioned
	int startIndex = (int)((pStart - segArray) / sizeof(T)) ;
	int curSeg = startIndex / sizeSegment ;

	if ( flagSeg[curSeg] == DS_SEG_FULL ) {
		sendSize = sizeSegment - (startIndex % sizeSegment) ;
	} else {
		// Do nothing
	}


	//** Step 2. Process rest segments
	T* dest = segSpace[!curSpace] + sizeSegment ;
	int destSize = 0 ;
	
	// Clean up reserved segments
	for ( int tID = 0 ; tID < numThread ; tID++ ) {
		flagSeg[segIndexFromPointer(reservedSeg[tID])] = DS_SEG_EMPTY ;
	}

	int threadID = 0 ;
	while ( curSeg < numSegment ) {
		switch ( flagSeg[curSeg] ) {

		case DS_SEG_ALLOCATED :
			threadID = ownerThread(curSeg) ;
			if ( threadID == -1 ) {
				printf("[Err]Compaction in segArray!") ;
				#ifdef _DEBUG
				getchar() ;
				#endif
			}
			sendSize = sendSize + indexInSeg[threadID] ;
			
		case DS_SEG_EMPTY :
			memcpy(dest, pStart, sendSize*sizeof(T)) ;
			dest = dest + sendSize ;
			destSize = destSize + sendSize ;
			sendSize = 0 ;
			pStart = segArray + (curSeg+1)*sizeSegment ;
			break ;

		case DS_SEG_FULL :
			sendSize = sendSize + sizeSegment ;
			break ;

		default :
			break ;
		}

		curSeg++ ;

	} // End while


	//** Step 3. Final send & Swap the space

	// Final send
	if ( sendSize > 0 ) {
		memcpy(dest, pStart, sendSize*sizeof(T)) ;
		destSize = destSize + sendSize ;
	}

	// Swap space
	curSpace = !curSpace ;
	segArray = segSpace[curSpace] ;
	curWaitingSeg = destSize / sizeSegment + 1 ;

	// Align the array
	sendSize = destSize % sizeSegment ;
	T* sendStart = segArray + (curWaitingSeg * sizeSegment) ;
	int startPosition = sizeSegment - sendSize ;
	
	pStart = segArray+startPosition ;
	numElements = destSize ;
	memcpy(pStart, sendStart, sendSize*sizeof(T)) ;

	for ( int i = 0 ; i < curWaitingSeg ; i++ ) {
		flagSeg[i] = DS_SEG_FULL ;
	}

	emptySeg = curWaitingSeg ;
	for ( int i = emptySeg ; i < numSegment ; i++ )
		flagSeg[i] = DS_SEG_EMPTY ;

	unsetLock() ;

	initialAlloc() ;

	return numElements ;

#else
	return 0 ;
#endif

}


/************************************************************************/
/* Interactions with producers                                          */
/************************************************************************/

template <class T>
inline void DS_SegArray<T>::requestSeg( int _id )
{
	segRequest[_id] = true ;
}

template <class T>
inline void DS_SegArray<T>::returnFullSeg( int _segID )
{
	flagSeg[_segID] = DS_SEG_FULL ;
}

template <class T>
inline void DS_SegArray<T>::returnFullSeg( T* _fullSeg )
{
	UINT index = segIndexFromPointer(_fullSeg) ;
	returnFullSeg(index) ;
}

template <class T>
void DS_SegArray<T>::setEmptySeg( int _startSeg, int _size )
{
	for ( int i = 0 ; i < _size ; i++ ) {
		flagSeg[_startSeg + i ] = DS_SEG_EMPTY ;
	}

	if (emptySeg < 0 )
		emptySeg = _startSeg ;
}

/************************************************************************/
/* Interactions with consumers                                          */
/************************************************************************/

template <class T>
int DS_SegArray<T>::finalGetElements( T** _start )
{
	setLock() ;

	int numFullSeg = 0 ;
	int startFullSeg ;
	while ( flagSeg[curWaitingSeg] != SEG_EMPTY ) {

		while ( flagSeg[curWaitingSeg] == DS_SEG_ALLOCATED ) {
			curWaitingSeg++ ;
		}

		if ( flagSeg[curWaitingSeg] != DS_SEG_FULL ) {
			unsetLock() ;
			return 0 ;
		}

		startFullSeg = curWaitingSeg ;
		while ( flagSeg[curWaitingSeg] == DS_SEG_FULL )
			curWaitingSeg++ ;
		numFullSeg = curWaitingSeg - startFullSeg ;

		*_start = &segArray[sizeSegment*startFullSeg] ;

		unsetLock() ;
		return numFullSeg*sizeSegment ;
	}

	unsetLock() ;
	return 0 ;
}

template <class T>
int DS_SegArray<T>::getNumElements( void ) {

	updateNumElements() ;
	return numElements ; 
}

template <class T>
int DS_SegArray<T>::getElements( int size, T** _start ) {

	setLock() ;

	*_start = pStart ;

	int returnSize ;
	if ( size > numElements ) {
		returnSize = numElements ;
	} else {
		returnSize = size ;
	}

	pStart = pStart + returnSize ;
	numElements -= returnSize ;

	unsetLock() ;

	return returnSize ;
}


template <class T>
bool DS_SegArray<T>::isEmpty( void )
{
	updateNumElements() ;
	return ( numElements == 0 ? true : false ) ;
}
