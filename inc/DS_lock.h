#pragma once
#include <omp.h>

class DS_lock
{
private:
	int			numLocks ;
	omp_lock_t* locks ;

public:

	DS_lock( int _numLocks = 1) {
		numLocks = _numLocks ;
		//SAFE_NEW(locks, omp_lock_t, numLocks);
		locks = new omp_lock_t[numLocks] ;
		initAllLock() ;
	} 

	~DS_lock() {
		for ( int i = 0 ; i < numLocks ; i++ )
			omp_destroy_lock(&locks[i]) ;
		delete [] locks ;
	}

	inline void initAllLock ( void ) {
		for ( int i = 0 ; i < numLocks ; i++ )
			omp_init_lock(&locks[i]) ;
	}

	inline void initLock(int _ID = 0) { omp_init_lock(&locks[_ID]) ; }
	inline void setLock(int _ID = 0) { omp_set_lock(&locks[_ID]) ; }
	inline void unsetLock(int _ID = 0) { omp_unset_lock(&locks[_ID]) ; }
} ;