#include "../inc/DS_hashedLock.h"

unsigned int DS_hashedLock::hash_div(unsigned int _in) {
	return _in%numLocks;
}


unsigned int DS_hashedLock::hash_knuth_div(unsigned int _in){
	return (_in*(_in + 3)) % numLocks;
}

int DS_hashedLock::setHashFunc(int _funcID){
	switch (_funcID){
	case DS_HASH_FUNC_DIVISION:
		hashFunc = &DS_hashedLock::hash_div;
		return DS_HASH_FUNC_DIVISION;

	case DS_HASH_FUNC_KNUTH_DIV:
		hashFunc = &DS_hashedLock::hash_knuth_div;
		return DS_HASH_FUNC_KNUTH_DIV;

	default:
		hashFunc = &DS_hashedLock::hash_div;
		return DS_HASH_FUNC_DIVISION;
	}
}