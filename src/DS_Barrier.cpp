#include "../inc/DS_Barrier.h"
#include <memory>

DS_Barrier::DS_Barrier(int _numThreads)
{
	numThreads = _numThreads ;
	flag = new bool[numThreads] ;
	memset(flag, 0, sizeof(bool)*numThreads) ;
	masterFlag = false ;
}

DS_Barrier::~DS_Barrier(void)
{
	delete flag ;
}

void DS_Barrier::initBarrier( void )
{
	memset(flag, 0, sizeof(bool)*numThreads) ;
	masterFlag = false ;
}

void DS_Barrier::checkAllReach( void )
{
	for ( int i = 0 ; i < numThreads ; i++ ) {
		if ( flag[i] == false )
			return ;
	}
	masterFlag = true ;
}

void DS_Barrier::checkAllPass( void )
{

	for ( int i = 0 ; i < numThreads ; i++ ) {
		if ( flag[i] == true )
			return ;
	}
	masterFlag = false ;
}

void DS_Barrier::reachBarrier( int tID )
{
	flag[tID] = true ;

	while ( true ) {
		if ( tID == 0 )
			checkAllReach() ;

		if ( isAllreach() )
			break ;
	}

	flag[tID] = false ;
	passBarrier( tID ) ;
}

void DS_Barrier::passBarrier( int tID )
{
	while ( true ) {
		if ( tID == 0 )
			checkAllPass() ;

		if ( isAllPass() )
			break ;
	}
}