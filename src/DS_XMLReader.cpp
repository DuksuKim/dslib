#include "../inc/DS_XMLReader.h"
#include "../inc/DS_common_def.h"

DS_XmlReader::DS_XmlReader(const char* _inputFileName)
{
	inputFileName = _inputFileName;
	parser = vtkXMLDataParser::New();
	parser->SetFileName(_inputFileName);
	if (parser->Parse() == 1)
	{
		xmlRootElement = parser->GetRootElement();
	}
	else
	{
		cout << "Fail to read the XML file - " << _inputFileName << endl;
		xmlRootElement = NULL;
		//EXIT_WIHT_KEYPRESS;
	}
}


DS_XmlReader::~DS_XmlReader()
{
	parser->Delete();
}

void DS_XmlReader::trimString(char* _str)
{
	char* curToRead = _str;
	char* curToWrite = _str;

	// Find start
	while (true){
		if (*curToRead == ' ')
			curToRead++;
		else
			break;
	}
	// Write
	while (true){
		if (*curToRead == ' ' || *curToRead == 0)
			break;
		*curToWrite = *curToRead;
		curToWrite++;
		curToRead++;
	}
	*curToWrite = '\0';

}

void DS_XmlReader::MakeUpperString(char* _str)
{
	int length = strlen(_str);
	LOOP_I(length){
		if (_str[i] > 'a')
			_str[i] -= 32;
	}
}

vtkXMLDataElement*
DS_XmlReader::safe_GetElementWithName(vtkXMLDataElement* _curEle, char* _name)
{
	if (_curEle->FindNestedElementWithName(_name) != NULL)
	{
		return _curEle->FindNestedElementWithName(_name);
	}
	cout << "Parsing error - cannot find '" << _name << "'" << endl;
	return NULL;
}

bool DS_XmlReader::loadDataBool(vtkXMLDataElement* _curEle, bool& _dest, char* _key)
{
	vtkXMLDataElement* curEle = _curEle;

	if (_key)
		curEle = safe_GetElementWithName(_curEle, _key);

	if (curEle){
		char* curString = curEle->GetCharacterData();
		trimString(curString);
		MakeUpperString(curString);
		if (strcmp(curString, "TRUE") == 0){
			_dest = true;
			return true;
		}
	}
	_dest = false;
	return false;
}

bool DS_XmlReader::loadDataInt(vtkXMLDataElement* _curEle, int& _dest, char* _key)
{
	vtkXMLDataElement* curEle = _curEle;

	if (_key)
		curEle = safe_GetElementWithName(_curEle, _key);

	if (curEle){
		_dest = atoi(curEle->GetCharacterData());
		return true;
	}

	return false;
}

bool DS_XmlReader::loadDataFloat(vtkXMLDataElement* _curEle, float& _dest, char* _key)
{
	vtkXMLDataElement* curEle = _curEle;

	if (_key)
		curEle = safe_GetElementWithName(_curEle, _key);

	if (curEle){
		_dest = (float)atof(curEle->GetCharacterData());
		return true;
	}

	return false;
}

bool DS_XmlReader::loadDataString(vtkXMLDataElement* _curEle, char* _dest, char* _key)
{
	vtkXMLDataElement* curEle = _curEle;

	if (_key)
		curEle = safe_GetElementWithName(_curEle, _key);

	if (curEle){
		char* curString = curEle->GetCharacterData();
		trimString(curString);
		strcpy_s(_dest, 255, curString);
		//strcpy(_dest, curString);
		return true;
	}

	return false;
}