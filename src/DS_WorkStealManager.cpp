#include "../inc/DS_WorkStealManager.h"

DS_WorkStealManager::DS_WorkStealManager(int _numThreads)
{
	numThreads = _numThreads;
	requestFlag = NULL;
	finishedFlag = NULL;

	SAFE_NEW(requestFlag, bool*, numThreads);
	LOOP_I(numThreads){
		requestFlag[i] = NULL;
		SAFE_NEW(requestFlag[i], bool, numThreads);
	}
	SAFE_NEW(finishedFlag, bool, numThreads);
	initManager();
}


DS_WorkStealManager::~DS_WorkStealManager()
{
	LOOP_I(numThreads)
		SAFE_DELETE(requestFlag[i]);

	SAFE_DELETE(requestFlag);
	SAFE_DELETE(finishedFlag);
}

void DS_WorkStealManager::initManager(void)
{
	LOOP_I(numThreads)
		memsetZero<bool>(&requestFlag[i], numThreads);
	memsetZero<bool>(&finishedFlag, numThreads);
}

bool DS_WorkStealManager::isAllFinished(void){
	lock.setLock();
	LOOP_I(numThreads){
		if (!finishedFlag[i]) {
			lock.unsetLock();
			return false;
		}
	}
	lock.unsetLock();
	return true;
}

void DS_WorkStealManager::setFinished(UINT _tid){
	lock.setLock();
	finishedFlag[_tid] = true;
	lock.unsetLock();
}

void DS_WorkStealManager::unsetFinished(UINT _tid){
	lock.setLock();
	finishedFlag[_tid] = false;
	lock.unsetLock();
}

void DS_WorkStealManager::sendRequest(UINT _from, UINT _to){
	requestFlag[_to][_from] = true;
}

int DS_WorkStealManager::checkRequest(UINT _tid){
	LOOP_I(numThreads){
		if (requestFlag[_tid][i]) {
			requestFlag[_tid][i] = false;
			return i;
		}
	}
	return -1;
}