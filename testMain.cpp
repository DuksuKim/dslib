#include <DS_library.h>
#include <stdio.h>

int main ( void ) {

	printf("testMain for DS_libraries\n") ;

	DS_arrayQueue<int> *aQ = new DS_arrayQueue<int>(100, true) ;
	int data[100] = {0} ;
	for ( int i = 0 ; i < 100 ; i++ ) {
		aQ->push_back((void*)&i) ;
		printf("Push %d, remaining space = %d\n", i, aQ->remainSpace() ) ;
	}

	printf("----------------\n") ;

	for ( int i = 0 ; i < 20 ; i++ ) {
		aQ->pop_front((void*)&data[i]) ;
		printf("Pop %d, remaining space = %d\n", data[i], aQ->remainSpace() ) ;
	}

	printf("----------------\n") ;

	int* geter ;
	aQ->getChunk_back(20, (void**)&geter) ;
	printf("Get chuck(20)\n") ;
	for ( int i = 0 ; i < 20 ; i++ ) {
		printf("%d ", geter[i]) ;
	}

	printf("\n");

	printf("----------------\n") ;

	aQ->compaction() ;
	printf("Packing, remaining space = %d\n", aQ->remainSpace() ) ;

	printf("----------------\n") ;

	while ( !aQ->isEmpty() ) {
		int abc ;
		aQ->pop_front((void*)&abc) ;
		printf("Pop %d, remaining space = %d\n", abc, aQ->remainSpace() ) ;
	}

	getchar() ;
	return 0 ;
}

//#endif